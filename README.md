# BitBucket Pipelines for your CI

- APP PASSWORD: 8GYF5BdrczMRmrFTZDub

## What does it do
 * Validates the ``composer.json`` file

## Testing the Pipeline Locally
```
docker run -it -v $(pwd):/var/www/$(cwd) --workdir="/var/www/$(cwd)" --memory=4g --memory-swap=4g --entrypoint=/bin/bash php:7.1.1
```
Then run each command in your ``bitbucket-pipelines.yml`` manually in the terminal

----

### Breaking down the pipeline

``composer validate --no-check-all`` Validates the composer file and skips over the check for exact version numbers


### Validating your pipeline
copy and paste your pipeline here
https://bitbucket-pipelines.atlassian.io/validator


### SSH and Pipelines
https://community.atlassian.com/t5/Bitbucket-questions/How-can-I-use-SSH-in-Bitbucket-Pipelines/qaq-p/347345


### Publishing Build Articfacts
1. https://confluence.atlassian.com/bitbucket/publish-and-link-your-build-artifacts-872137736.html
1. https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html
1. https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D#post

https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/
export BB_AUTH_STRING=craigiswayne:atdYVpt9hKpC5jtCD2VQ;
https://craigiswayne:8GYF5BdrczMRmrFTZDub@api.bitbucket.org/2.0/repositories/craigiswayne/pipelines-playground/pipelines/
https://craigiswayne:8GYF5BdrczMRmrFTZDub@api.bitbucket.org/2.0/repositories/24dotcom


-----

## TODO
 * Validate the ``bower.json`` if it exists
 * Validate the ``package.json`` if it exists (npm doctor)
 * Validate the ``package-lock.json`` if it exists
 * Validate the ``Gruntfile.js`` if it exists
 * PHP lint all php files
 * JS lint all js files
 * CSS lint all css files
 * There should be no git changes, otherwise throw an error
 * Attempt to run grunt
 * Attempt to run node install
 * Check for the presence of CONTRIBUTING.md, README.md, CHANGELOG.md
 * Fail if the only change to a file is the white space
 * Authentication on composer install see this [issue](https://github.com/composer/composer/issues/4285)
 * Lint only 24.com themes
 * Lint only 24.com plugins
 * Lint SaSS files (https://www.npmjs.com/package/sass-lint)
 * Show all TODO's in a project (maybe this should only run on a maintenance branch????)
 * composer authentication (https://getcomposer.org/doc/06-config.md#gitlab-oauth)
 * https://getcomposer.org/doc/articles/troubleshooting.md#api-rate-limit-and-oauth-tokens
 * composer diagnose throws an error, lets try not make it throw an error
 * composer diagnose --no-interaction
 * Delete all remote branches merged into develop (try this: git branch -r --merged | grep origin | egrep -v '>|develop' | cut -d/ -f2- | xargs git push origin --delete )
 * Run relevant QA Tests ( see below for tests )
 * Show all repos with branches that have unmerged releases
 * Throw error forcfully [https://stackoverflow.com/questions/30078281/raise-error-in-bash-script](https://stackoverflow.com/questions/30078281/raise-error-in-bash-script)
 * Separate code for pipelines to curl from single repo and then use those commands for each branch
 * https://confluence.atlassian.com/bitbucket/run-pipelines-manually-861242583.html


## Tests
 * Build project and look for any code that ``http://`` or ``https://`` in them, it should replace with just ``//`` (So that it can work on insecure and secure protocols)
 * ``wp_enqueue_script`` should always use the version for that plugin or theme

## Repository Checks
 Throw warnings in build if the repository is missing the following

 1. Avatar
 1. The Repository is missing a develop branch
 1. The Repository is missing a master branch
 1. The Repository is missing a hipchat integration
 1. The Repository is missing a wiki integration
 1. The Repository is missing a README.md
 1. The Repository is missing a CHANGELOG.md


 Make use of phpunit tests
 ```
    - composer require phpunit/phpunit
    - vendor/bin/phpunit
 ```


 ## Barebones Setup
 ```sh
brew install docker docker-compose docker-machine;
brew cask install docker;

# Sor if you already have docker installed
brew upgrade docker

# Setup docker from the app
open -a Docker

# If your webroot folder is not in either /Users/, /Volumes/, /private/, and /tmp
# See here for adding it to the filesharing preferences: https://docs.docker.com/docker-for-mac/osxfs/#namespaces
```
